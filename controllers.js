'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);
//
phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
  function($scope, Phone) {
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
  }]);

//detailCtrl
phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);

//compareCtrl
phonecatControllers.controller('PhoneCompareCtrl', ['$scope','$routeParams','$http',
function($scope,$routeParams,$http){
        $http.get('phones/' + $routeParams.phone1Id+'.json').success(function(data){
                 $scope.phone1 = data;
    });
         $http.get('phones/' + $routeParams.phone2Id + '.json').success(function(data) {
                 $scope.phone2 = data;
    });
  }]);
